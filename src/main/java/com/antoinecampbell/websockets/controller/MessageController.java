package com.antoinecampbell.websockets.controller;

import com.antoinecampbell.websockets.model.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 *
 */
@Controller
public class MessageController {

    @MessageMapping("/hello")
    @SendTo("/topic/messages")
    public Message message(Message message) {
        return new Message(String.format("Echo: %s", message.getName()));
    }
}
